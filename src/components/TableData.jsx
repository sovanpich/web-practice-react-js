import React from 'react'
import { Container,Table } from 'react-bootstrap'

function TableData({items}) {
    return (
        <Container>
            <h1>Table</h1>
            <Table striped bordered hover>
                <thead>
                    <tr>
                    <th>#</th>
                    <th>Food</th>
                    <th>Amount</th>
                    <th>Price</th>
                    <th>Total</th>
                    </tr>
                </thead>
                 {
                items.map((item,idx)=>(
                <tbody>
                    <tr>
                    <th>{item.id}</th>
                    <th>{item.title}</th>
                    <th>{item.amount}</th>
                    <th>{item.price}</th>
                    <th>{item.total}</th>
                    </tr>
                </tbody>
                    ))
                }
                </Table>
        </Container>
    )
}

export default TableData
